//
//  UIColor+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit

extension UIColor {
    
    // MARK: - Public Methods
    
    convenience init(r: Int, g: Int, b: Int) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: 1.0)
    }
    
    convenience init(r: Int, g: Int, b: Int, alpha: CGFloat) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: alpha)
    }
    
    static var firstTutorialTintColor: UIColor {
        return UIColor(red: 104.0/255.0, green: 170.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    }
    
    static var secondTutorialTintColor: UIColor {
        return UIColor(red: 187.0/255.0, green: 152.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    }
    
    static var thirdtutorialTintColor: UIColor {
        return UIColor(red: 126.0/255.0, green: 216.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    }
    
    static var forthTutorialTintColor: UIColor {
        return UIColor(red: 227.0/255.0, green: 201.0/255.0, blue: 167.0/255.0, alpha: 1.0)
    }
    
    @nonobjc class var borderGrey: UIColor {
        return UIColor(red: 188.0 / 255.0, green: 187.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var greenMint: UIColor {
        return UIColor(red: 84.0 / 255.0, green: 205.0 / 255.0, blue: 95.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appTint: UIColor {
        return UIColor(red: 30.0 / 255.0, green: 183.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightGrayBackground: UIColor {
        return UIColor(white: 245.0 / 255.0, alpha: 1.0)
    }
    
    static var errorTintColor: UIColor {
        return UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    
//    static var lightGrayBackground : UIColor {
//         return UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
//    }
}

// MARK: - UIColor+Hex

extension UIColor {
    
    convenience init(hexString: String, alpha: CGFloat = 1) {
        var hex: UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        scanner.scanHexInt32(&hex)
        self.init(hex: hex)
    }
    
    convenience init(hex: UInt32, alpha: CGFloat = 1) {
        self.init(red: CGFloat((hex >> 16) & 0xFF) / 255,
                  green: CGFloat((hex >> 8) & 0xFF) / 255,
                  blue: CGFloat(hex & 0xFF) / 255,
                  alpha: alpha)
    }
    
    convenience init(hexWithAlpha: UInt64) {
        self.init(red: CGFloat((hexWithAlpha >> 24) & 0xFF) / 255,
                  green: CGFloat((hexWithAlpha >> 16) & 0xFF) / 255,
                  blue: CGFloat((hexWithAlpha >> 8) & 0xFF) / 255,
                  alpha: CGFloat(hexWithAlpha & 0xFF) / 255)
    }
    
}
