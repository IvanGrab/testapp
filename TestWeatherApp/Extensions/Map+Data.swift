//
//  Map+Data.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Foundation

extension Map {
    
    convenience init?(data: Data) {
        guard let jsonData = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any], let json = jsonData["data"] as? [String : Any]  else { return nil }
        self.init(mappingType: .fromJSON, JSON: json)
    }
    
    convenience init?(withoutdataKeyData: Data) {
        guard let json = (try? JSONSerialization.jsonObject(with: withoutdataKeyData, options: .allowFragments)) as? [String: Any] else { return nil }
        self.init(mappingType: .fromJSON, JSON: json)
    }
    
}
