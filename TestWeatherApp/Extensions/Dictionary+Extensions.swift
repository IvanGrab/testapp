//
//  Dictionary+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

/**
 Returns a new dictionary with the elements of both these dictionaries.
 
 - Parameters:
 - left: A dictionary.
 - right: Another dictionary.
 
 - Returns: A new dictionary.
 */

func + <K,V>(left: [K:V], right: [K:V]) -> [K:V] {
    var map = [K:V]()
    for (k, v) in left {
        map[k] = v
    }
    for (k, v) in right {
        map[k] = v
    }
    return map
}

/**
 Add the elements to the dictionary.
 
 - Parameters:
 - left: The target dictionary.
 - right: Another dictionary.
 */

func += <K,V>( left: inout [K:V], right: [K:V]) {
    left = left + right
}
