//
//  Date+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Foundation

extension Date {
    
    static var shortDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateStyle = .medium
        return formater
    }
    
    static var monthFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "MM"
        return formater
    }
    static var yearFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy"
        return formater
    }
    
    static var monthDayYearBySlashFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "MM/dd/yyyy"
        return formater
    }
    
    static var hoursFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "hh"
        return formater
    }
    
    static var minutesFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "mm"
        return formater
    }
    
    var timeDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "hh:mm"
        return formater
    }
    
    static var timeDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "hh:mm"
        return formater
    }
    
    static var dayShortDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "EEE. dd MMM."
        return formater
    }
    
    static var shortLocaleDateFormater: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "dd MMM YYYY"
        formater.locale = NSLocale(localeIdentifier: "fr") as Locale!
        return formater
    }
    
    var cvvFormat: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "MM/YYY"
        return formater
    }
    
    var viaSlashFormat: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/YY"
        return formater
    }
    
    var birthdayFormat: DateFormatter {
        let formater = DateFormatter()
        formater.dateFormat = "dd MMM YYY"
           formater.locale = NSLocale(localeIdentifier: "fr") as Locale!
        return formater
    }
    
    static func timeDateFormaterWith(format: FormatStrings) -> DateFormatter {
        let dateFormatter = timeDateFormater
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter
    }
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour) hour ago" : "\(hour) hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute) minute ago" : "\(minute) minutes ago"
        } else {
            return "a moment ago"
        }
        
    }
    
    func getRemainingDays() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: Date(), to: self)
        var days = 0
        var hours = 0
        if let year = interval.year, year > 0 {
            for _ in 1...year {
                days += Calendar.current.range(of: .day, in: .year, for: self)?.count ?? 0
            }
        }
        if let month = interval.month, month > 0 {
            days += Calendar.current.range(of: .day, in: .month, for: self)?.count ?? 0
        }
        if let day = interval.day, day > 0 {
            days += day
        }
        if let hour = interval.hour, hour > 0 {
            hours += hour
        } else if let minute = interval.minute, minute > 0 {
            return "\(minute) " + NSLocalizedString("m", comment: "")
        } else {
            return NSLocalizedString("Moins de 1 m.", comment: "")
        }
        return "\(days)" + NSLocalizedString("j. ", comment: "") + NSLocalizedString("et ", comment: "") + "\(hours)" + NSLocalizedString("h", comment: "")
    }
    
    func getDayMonth() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: Date(), to: self)
        var hours = 0
        if let hour = interval.hour, hour > 24 {
           return Date.shortLocaleDateFormater.string(from: self)
        } else {
            return self.timeDateFormater.string(from: self)
        }
    }
    
    
    func formatedPubDate() -> String? {
        let dateFormatter = DateFormatter()
        if Calendar.current.isDateInToday(self) {
            dateFormatter.dateFormat = FormatStrings.hoursMinutes24.rawValue
        } else {
            dateFormatter.dateFormat = FormatStrings.monthShortDayYear.rawValue
        }
        
        return dateFormatter.string(from: self)
    }
    
}

extension Date {
    
    enum FormatStrings: String {
        case hoursMinutes24 = "hh:mm"
        case hoursMinutes12 = "hh:mm a"
        case monthDayYear = "MMMM d, yyyy"
        case monthShortDayYear = "MMM d, yyyy"
        case monthDayYearHoursMinutes12 = "MMMM d, yyyy hh:mm a"
        case hoursMinutesMonthDayYear12 = "hh:mm a MMMM d, yyyy"
        case hoursMinutesMonthDayYear24 = "hh:mm MMMM d, yyyy"
        case yearMonthDayHoursMinutesSeconds24 = "yyyy-MM-dd HH:mm:ss"
    }
    
}
