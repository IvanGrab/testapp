//
//  UIStoryboard+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit

extension UIStoryboard {
    
    enum Storyboard: String {
        case Main
        case Authentication
    }
    
    // MARK: - Convenience Initializers
    
    convenience init(_ storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    // MARK: - Public Methods
    
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = self.instantiateViewController(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("> UIStoryboard: Couldn't instantiate view controller with identifier \(String(describing: T.self)) ")
        }
        return viewController
    }
    
}
