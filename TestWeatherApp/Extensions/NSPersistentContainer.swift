//
//  NSPersistentContainer.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import CoreData

extension NSPersistentContainer {
    // MARK: - Class Methods
    
    class func `default`() -> NSPersistentContainer {
        let persistentContainer = NSPersistentContainer(name: "TestWeatherApp")
        
        persistentContainer.loadPersistentStores { (description, error) in
            if let error = error {
                print("> NSPersistentContainer: could not initialize a DB due to \(error.localizedDescription).")
            } else {
                print("> NSPersistentContainer: a DB directory has been set to \(NSPersistentContainer.defaultDirectoryURL().path).")
            }
        }
        
        return persistentContainer
    }
}
