//
//  UIViewController+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var isVisible: Bool {
        return isViewLoaded && view.window != nil
    }
    
}

// MARK: - Error handling

extension UIViewController {
    
    var noInternetConnectionHandler: (_ request: URLRequest) -> Void {
        return { [weak self] request in
            self?.showNoInternetConnectionAlert()
        }
    }
    
    var requestFailureHandler: (_ error: RESTClient.ErrorType, _ data: Data?, _ request: URLRequest) -> Void {
        return { [weak self] (error, data, request) in
            self?.handleFailedRequest(error: error, data: data)
        }
    }
    
    func showNoInternetConnectionAlert() {
        let title = NSLocalizedString("No internet connection", comment: "")
        let message = NSLocalizedString("Your internet connection appears to be down. Please check and try again.", comment: "")
        let noInternetError = RESTClient.ErrorType.serverError(errorJSON: ["message" : message as AnyObject])
        guard presentedViewController == nil else {
            if let activity = presentedViewController as? ActivityViewController {
                activity.dismiss(animated: true, completion: {
                    //MARK: - show error
                })
            }
            return
        }
    }
    
    func handleFailedRequest(error: RESTClient.ErrorType, data: Data?) {
        guard presentedViewController == nil else {
            if let activity = presentedViewController as? ActivityViewController {
                activity.dismiss(animated: true, completion: {
                    ShowAlert.infoAlert(in: self, title: "Error", message: error.description, okActionTitle: "OK")
                })
            }
            return
        }
        //MARK: - show error alert
    }
    
    func errorMessageFrom(data: Data?) -> String {
        guard let data = data, data.count > 0, let errorJSON = Map(withoutdataKeyData: data)?.JSON as [String: AnyObject]? else {
            return NSLocalizedString("Unknown server error", comment:"")
        }
        
        var message = ""
        if let serverErrorMessage = errorJSON["message"] as? String {
            message = serverErrorMessage + "\n"
            if let statusCode = errorJSON["status_code"] as? Int {
                message.append("Status Code: \(statusCode)\n")
            }
        } else if let dictionary = errorJSON as? [String: [String]] {
            for (_, value) in dictionary {
                for (_, shortMessage) in value.enumerated() {
                    message.append(shortMessage + "\n")
                }
            }
        } else if let remainingTime = errorJSON["remaining_time"] as? Int {
            let minutes = Int(remainingTime)
            message = NSLocalizedString("You are blocked for ", comment: "") + String(minutes) + NSLocalizedString(" minutes.", comment: "") + "\n"
        }
        let cuttedMessage = String(message.characters.dropLast())
        message = cuttedMessage.characters.count > 0 ? cuttedMessage : NSLocalizedString("Unknown server error", comment:"")
        
        return message
    }

}
extension UIViewController {
    
    func setUpDefaultNavigationBar() {
        navigationController?.navigationBar.shadowImage = nil
    }
    
}
