//
//  Data+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Foundation

extension Data {
    
    var prettyJSONString: String {
        if let json = try? JSONSerialization.jsonObject(with: self, options: .allowFragments), JSONSerialization.isValidJSONObject(json), let formatted = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), let string = String(data: formatted, encoding: .utf8) {
            return string
        }
        return String(data: self, encoding: .utf8) ?? ""
    }
    
}
