//
//  UIView+Extensions.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

// MARK: - UIView+ActivityIndicator

extension UIView {
    
    // MARK: - Properties
    
    var isActive: Bool {
        get {
            return isUserInteractionEnabled
        }
        set {
            isUserInteractionEnabled = newValue
        }
    }
    
    private static var activityIndicatorAssociatedKey = "activityIndicatorAssociatedKey"
    
    private var activityIndicator: UIActivityIndicatorView? {
        get {
            return objc_getAssociatedObject(self, &UIView.activityIndicatorAssociatedKey) as? UIActivityIndicatorView
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &UIView.activityIndicatorAssociatedKey, newValue as UIActivityIndicatorView?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    private var newActivityIndicator: UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.color = UIColor.black
        return activityIndicator
    }
    
    // MARK: - Public
    
    func addShadow(with radius: CGFloat = 3, opacity: Float = 0.5, color: UIColor? = .black, shadowOfset: CGSize = .zero) {
        layer.shadowColor = color?.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = shadowOfset
        layer.shadowRadius = radius
        layer.masksToBounds = false
    }

    
    func showwActivityIndicator() {
        DispatchQueue.main.async {
            self.isActive = false
            self.activityIndicator = self.newActivityIndicator
            self.activityIndicator!.startAnimating()
            self.activityIndicator!.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(self.activityIndicator!)
            
            let centerY = NSLayoutConstraint(item: self.activityIndicator!,
                                             attribute: NSLayoutAttribute.centerY,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: self,
                                             attribute: NSLayoutAttribute.centerY,
                                             multiplier: 1,
                                             constant: 0)
            self.superview?.addConstraint(centerY)
            
            let centerX = NSLayoutConstraint(item: self.activityIndicator!,
                                             attribute: NSLayoutAttribute.centerX,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: self,
                                             attribute: NSLayoutAttribute.centerX,
                                             multiplier: 1,
                                             constant: 0)
            self.superview?.addConstraint(centerX)
        }
    }
    
    func hideeActivityIndicator(andActivate activate: Bool = true) {
        DispatchQueue.main.async {
            self.isActive = activate
            guard let activityIndicator = self.activityIndicator else { return }
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}
