//
//  UIImageView+ImageDownloading.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

extension UIImageView {
    
    typealias PlaceholderSettings = (placeholderContentMode: UIViewContentMode?, placeholderTintColor: UIColor?)
    
    // MARK: - Public
    func setImage(with avatar: Avatar?, placeholder: UIImage? = nil, using activityIndicatorStyle: UIActivityIndicatorViewStyle? = .gray, placeholderSettings: PlaceholderSettings? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
        if let image = avatar?.image {
            self.image = image
        } else if let imagePath = avatar?.relativePath {
            setImage(with: imagePath, placeholder: placeholder, using: activityIndicatorStyle, placeholderSettings: placeholderSettings, completion: { (success) in
                if success {
                    avatar?.image = self.image
                }
                completion?(success)
            })
        }
    }
    
    func setImage(with path: String?, placeholder: UIImage? = nil, using activityIndicatorStyle: UIActivityIndicatorViewStyle? = .gray, placeholderSettings: PlaceholderSettings? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
        
        var url: URL?
        if let path = path {
            if path.contains(Configuration.restAPIURL.absoluteString) {
                url = URL(string: path)
            } else {
                url =  Configuration.restAPIURL.appendingPathComponent(path)
            }
        }
        
        setImage(with: url, placeholder: placeholder, using: activityIndicatorStyle, completion: completion)
    }
    
    func setImage(with url: URL?, placeholder: UIImage? = nil, using activityIndicatorStyle: UIActivityIndicatorViewStyle? = .gray, placeholderSettings: PlaceholderSettings? = nil, completion: ((_ success: Bool) -> Void)? = nil) {
        
        let originalContentMode = contentMode
        let originalTintColor = tintColor
        
        if placeholder != nil {
            if let placeholderContentMode = placeholderSettings?.placeholderContentMode {
                contentMode = placeholderContentMode
            }
            if let placeholderTintColor = placeholderSettings?.placeholderTintColor {
                tintColor = placeholderTintColor
            }
            image = placeholder
        }
        
        let performCompletion: (_ success: Bool) -> Void = { [weak self] (success: Bool) in
            if success, placeholder != nil, let placeholderSettings = placeholderSettings {
                if placeholderSettings.placeholderContentMode != nil {
                    self?.contentMode = originalContentMode
                }
                if placeholderSettings.placeholderTintColor != nil {
                    self?.tintColor = originalTintColor
                }
            }
            if activityIndicatorStyle != nil {
                self?.removeActivityIndicator()
            }
            completion?(success)
        }
        
        guard let url = url else {
            performCompletion(false)
            return
        }
        
        if let activityIndicatorStyle = activityIndicatorStyle {
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: activityIndicatorStyle)
            activityIndicator.center = CGPoint(x: bounds.midX, y: bounds.midY)
            activityIndicator.startAnimating()
            addSubview(activityIndicator)
        }
        
        UIImage.load(url: url) { [weak self] (image) in
            guard let image = image else {
                DispatchQueue.main.async {
                    performCompletion(false)
                }
                return
            }
            DispatchQueue.main.async {
                self?.image = image
                performCompletion(true)
            }
        }
        
    }
    
    // MARK: - Private
    
    private func removeActivityIndicator() {
        for view in subviews {
            if view is UIActivityIndicatorView {
                view.removeFromSuperview()
            }
        }
    }
    
}

