//
//  AddItemTableViewController.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation
class AddItemTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageDescription: UITextField!
    @IBOutlet weak var hashtag: UITextField!
    
    var item = WheatherItem()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        item.onRequestFailure = requestFailureHandler
        
        
        LocationManager.shared.updateLocationWith(coordinateCompletionHandler: { (location) in
            if let coord = location?.coordinate {
                self.item.coordinates = coord
            }
            LocationManager.shared.stopLocationUpdates()
        }, authStatusCompletionHandler: locationManagerAuthStateDidChange(_:))
    }

    func locationManagerAuthStateDidChange(_ status: CLAuthorizationStatus?) {
        switch status {
        case .restricted?, .denied?:
            ShowAlert.infoAlert(in: self, title: "Eror", message: "Please alow access to using your location!", okActionTitle: "OK")
        default: break
        }
        if status != CLAuthorizationStatus.authorizedWhenInUse {
            print("> AddItemTableViewController: Location access is denied!")
        }
    }
    
}

extension AddItemTableViewController: PickingImage {
    
    func didSelectMedia(with image: UIImage?, thumbnail: UIImage?) {
        if let image = image {
            imageView.image = image
            let avatar = Avatar()
            avatar.image = image
            item.image = avatar
        }
    }
    
    func pickingDidCancel() {
        
    }
    
}

fileprivate extension AddItemTableViewController {
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        item.imageDescription = imageDescription.text
        item.hashtag = hashtag.text
        
        do {
            try item.validate()
            present(activityVC, animated: true, completion: nil)
            item.post { (success) in
                self.activityVC.dismiss(animated: true, completion: {
                    if success {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            }
        } catch {
            let arror = error as! ValueValidationError
            ShowAlert.infoAlert(in: self, title: "Error", message: arror.localizedDescription, okActionTitle: "OK")
        }
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        startPicking(isCameraOnly: false)
    }
    
}
