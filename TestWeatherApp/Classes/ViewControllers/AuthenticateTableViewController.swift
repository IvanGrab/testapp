//
//  AuthenticateTableViewController.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit

final class AuthenticateTableViewController: BaseTableViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            userImageView.layer.cornerRadius = userImageView.bounds.height / 2
        }
    }
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var actionTypeSegmentedControll: UISegmentedControl!
    
    //MARK: - Properties
    
    var currentType: AuthType = .login {
        didSet {
            userImageView.isHidden = currentType == .login
            actionButton.setTitle(currentType == .login ? "Login" : "SignUp", for: .normal)
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    var registrationModel: RegisterModel = RegisterModel()
    var loginModel: LoginModel = LoginModel()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentType = .login
        sessionManager.onRequestFailure = requestFailureHandler
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath {
        case IndexPath(row: 1, section: 0):
            return currentType == .login ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        username.resignFirstResponder()
        email.resignFirstResponder()
        password.resignFirstResponder()
    }
 
    
    
}

extension AuthenticateTableViewController: PickingImage {
    
    func didSelectMedia(with image: UIImage?, thumbnail: UIImage?) {
        if let image = image {
            userImageView.image = image
            registrationModel.avatar = image
        }
    }
    
    func pickingDidCancel() {
        
    }
    
}

extension AuthenticateTableViewController {
    
    enum AuthType: Int {
        case login
        case sigup
    }
    
}

fileprivate extension AuthenticateTableViewController {
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        if currentType == .login {
            loginModel.email = email.text
            loginModel.password = password.text
            do {
                try loginModel.validate()
                present(activityVC, animated: true, completion: nil)
                sessionManager.login(with: loginModel) { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            UIWindow.replaceStoryboard(name: .Main)
                        }
                    })
                }
            } catch {
                let errro = error as! ValueValidationError
                ShowAlert.infoAlert(in: self, title: "Error", message: errro.localizedDescription, okActionTitle: "OK")
            }
        } else {
            registrationModel.username = username.text
            registrationModel.email = email.text
            registrationModel.password = password.text
            
            do {
                try registrationModel.validate()
                present(activityVC, animated: true, completion: nil)
                sessionManager.signUp(with: registrationModel) { (success) in
                    self.activityVC.dismiss(animated: true, completion: {
                        if success {
                            UIWindow.replaceStoryboard(name: .Main)
                        }
                    })
                }
            } catch {
                let errro = error as! ValueValidationError
                ShowAlert.infoAlert(in: self, title: "Error", message: errro.localizedDescription, okActionTitle: "OK")
            }
        }
    }
    
    @IBAction func typeChanged(_ sender: UISegmentedControl) {
        if let type = AuthType(rawValue: sender.selectedSegmentIndex) {
            currentType = type
        }
    }
    
    @IBAction func avatarTapped(_ sender: UITapGestureRecognizer) {
        startPicking(isCameraOnly: false)
    }
}
