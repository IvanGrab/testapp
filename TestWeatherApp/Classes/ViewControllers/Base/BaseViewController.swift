//
//  BaseViewController.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit


class BaseViewController: UIViewController {
    
    // MARK: - Properties
    
    var sessionManager: SessionManager {
        return AppDelegate.shared.sessionManager
    }
    
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subscribeOnNotifications()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupHandlers()
        setUpDefaultNavigationBar()
    }
    
    deinit {
        unsubscribeOnNotifications()
    }
    
    func setupHandlers() {
        sessionManager.onRequestFailure = self.requestFailureHandler
    }
    
    func subscribeOnNotifications() {
        
    }
    
    func unsubscribeOnNotifications() {
        
    }
    
}
