//
//  BaseTableViewController.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit

class BaseTableViewController: UITableViewController {
    
    // MARK: - Properties

    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    var sessionManager: SessionManager {
        return AppDelegate.shared.sessionManager
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeOnNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupHandlers()
        setUpDefaultNavigationBar()
    }
    
    deinit {
        unsubscribeOnNotifications()
    }
    
    func setupHandlers() {
        
    }
    
    // MARK: - Autorefreshable
    
    @IBAction func refreshContent(_ sender: UIRefreshControl?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            sender?.endRefreshing()
        }
    }
    
    func subscribeOnNotifications() {
        
    }
    
    func unsubscribeOnNotifications() {
        
    }
    
}

// MARK: - Public
// MARK: - Private
