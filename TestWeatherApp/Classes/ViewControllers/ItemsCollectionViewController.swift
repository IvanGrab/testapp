//
//  ItemsCollectionViewController.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation

private let reuseIdentifier = "Cell"

class ItemsCollectionViewController: UICollectionViewController {

    //MARK: - IBOutlets
    
    var items = [WheatherItem]()
    
    lazy var activityVC: ActivityViewController = {
        return ActivityViewController(viewController: self, sourceVeiw: nil)
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layot = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        self.collectionView?.collectionViewLayout = layot
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        WheatherItem.getAllImages { (items, error) in
            DispatchQueue.main.async {
                if let datasourse = items {
                    self.items = datasourse
                    self.collectionView?.reloadData()
                } else {
                    ShowAlert.infoAlert(in: self, title: "Error", message: error?.localizedDescription, okActionTitle: "OK")
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        cell.imageView.setImage(with: item.image, placeholder: #imageLiteral(resourceName: "placeholder"), using: .gray, placeholderSettings: nil, completion: nil)
        cell.weatherLabel.text = item.weather
        cell.addresLabel.text = ""
        if let address = item.address {
            cell.addresLabel.text = address
        } else {
            DispatchQueue.global().async {
                self.getAddressFrom(item.coordinates!, { (address) in
                    DispatchQueue.main.async {
                        cell.addresLabel.text = address
                    }
                })
            }
        }
        return cell
    }

}

extension ItemsCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 0, 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width - 20
        return CGSize(width: width/2, height: width/2)
    }
    
}

fileprivate extension ItemsCollectionViewController {
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
        present(activityVC, animated: true, completion: nil)
        AppDelegate.shared.sessionManager.signOut { (success) in
            self.activityVC.dismiss(animated: true, completion: {
                if success {
                    UIWindow.replaceStoryboard(name: .Authentication)
                }
            })
        }
    }
    
    func getAddressFrom(_ coordinates: CLLocationCoordinate2D, _ completion:@escaping ((_ address: String?)->())) {
        let loc: CLLocation = CLLocation(latitude:coordinates.latitude, longitude: coordinates.longitude)
        let ceo: CLGeocoder = CLGeocoder()
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks?.first {
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    completion(addressString)
                }
        })
        
    }
}
