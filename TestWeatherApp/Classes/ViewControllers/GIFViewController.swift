//
//  GIFViewController.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class GIFViewController: BaseViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        present(activityVC, animated: true, completion: nil)
        WheatherItem.getGIFURL { (url, error) in
            if let url = url {
                DispatchQueue.main.async {
                    let _ = AppDelegate.shared.imageProvider.gifImageForPath(path: url, completion: { (image, imageData) in
                        if let image = image {
                            self.imageView.image = image
                        } else {
                            self.imageView.image = #imageLiteral(resourceName: "placeholder")
                        }
                        
                    })
                }
            } else {
                ShowAlert.infoAlert(in: self, title: "Error", message: "Something go wong", okActionTitle: "OK")
            }
        }
        
    }

}

fileprivate extension GIFViewController {
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
