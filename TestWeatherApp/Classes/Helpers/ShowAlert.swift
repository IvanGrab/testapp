//
//  ShowAlert.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

class ShowAlert: NSObject {
    
    class func showAlertContrller(with style: UIAlertControllerStyle, title: String?, message: String?, actions: [UIAlertAction], in controller: UIViewController, completion: (()->Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            alertController.addAction(action)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true, completion: completion)
    }
    
    class func showCancelActionAlert(in controller: UIViewController, title: String, message: String, okActionTitle: String, actionHandler: ((_ action: UIAlertAction)->())? = nil, cancelTitle: String, preferredStyle: UIAlertControllerStyle) {
        let alertController = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: ""), preferredStyle: preferredStyle)
        
        let okAction = UIAlertAction(title: NSLocalizedString(okActionTitle, comment: ""), style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            actionHandler?(action)
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString(cancelTitle, comment: ""), style: .cancel, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func infoAlert(in controller: UIViewController, title: String, message: String?, okActionTitle: String, actionHandler: ((_ action: UIAlertAction)->())? = nil) {
        let alertController = UIAlertController(title: NSLocalizedString(title, comment: ""), message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString(okActionTitle, comment: ""), style: .default, handler: { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            actionHandler?(action)
        })
        alertController.addAction(okAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
}
