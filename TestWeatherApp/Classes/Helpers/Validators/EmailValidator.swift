//
//  EmailValidator.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class EmailValidator: Validator {

    override func validate(_ value: Any?) throws {
        try super.validate(value)
        
        guard let value = value as? String, !value.isEmpty else {
            throw ValueValidationError.required("Email is required!")
        }
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        guard NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: value) else {
            throw ValueValidationError.invalid("Email is not valid!")
        }
    }
    
}
