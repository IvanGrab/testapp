//
//  PasswordValidator.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit

final class PasswordValidator: Validator {

    override func validate(_ value: Any?) throws {
        try super.validate(value)
        
        guard let value = value as? String, !value.isEmpty else {
            throw ValueValidationError.required(NSLocalizedString("Password is required!", comment: ""))
        }
        
        let regex = "^(?=.*\\w).{6,20}$"
        guard NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: value)
            else { throw ValueValidationError.invalid(NSLocalizedString("Password must be from 6 to 20 characters long!", comment: "")) }
    }
    
}
