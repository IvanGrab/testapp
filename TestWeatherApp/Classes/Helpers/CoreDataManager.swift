//
//  CoreDataManager.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    var managedObjectContext: NSManagedObjectContext!
    var managedObjectModel: NSManagedObjectModel?
    var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    
    // MARK: - Lifecycle
    
    init() {
        initializeCoreDataStack()
    }
    
    // MARK: - Public
    
    func saveContext(completion: NetworkModelCompletion? = nil) {
        if managedObjectContext.hasChanges {
            managedObjectContext.performAndWait({
                do {
                    try self.managedObjectContext.save()
                    completion?(true)
                    print("> CoreDataManager: Context Saved!")
                } catch {
                    completion?(false)
                    fatalError("Failed to save managedObjectContext: \(error)")
                }
            })
        }
    }
    
    func fetchObject(className: String, sortedBy propertyToSort: String, value: String?) -> ([NSManagedObject]?) {
        let searchFetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: String(describing: className.self), in: CoreDataManager.shared.managedObjectContext)
        searchFetchRequest.entity = entityDescription
        
        let predicate = NSPredicate(format: "\(propertyToSort) == %@", value ?? "")
        searchFetchRequest.predicate = predicate
        
        searchFetchRequest.returnsObjectsAsFaults = false
        
        var results: [NSManagedObject]? = nil
        do {
            results = try managedObjectContext.fetch(searchFetchRequest) as? [NSManagedObject]
        } catch {
            fatalError("Failed to perform fetch: \(error)")
        }
        
        return results
    }
    
    func fetchAllObjects(className: String) -> ([NSManagedObject]?) {
        guard let searchFetchRequest = managedObjectModel?.fetchRequestTemplate(forName: Constants.fetchRequestAllUsers) else { return nil}
        return execute(fetchRequest: searchFetchRequest)
    }
    
}

// MARK: - Private

private extension CoreDataManager {
    
    func initializeCoreDataStack() {
        guard let modelURL = Bundle.main.url(forResource: "TestWeatherApp", withExtension: "momd") else {
            fatalError("Failed to locate TestWeatherApp.momd in app bundle")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Failed to initialize managedObjectModel")
        }
        self.managedObjectModel = managedObjectModel
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        let concurrencyType = NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType
        managedObjectContext = NSManagedObjectContext(concurrencyType: concurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        guard let applicationDocumentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            fatalError("Failed to resolve documents directory")
        }
        let storeURL = applicationDocumentsDirectoryURL.appendingPathComponent("TestWeatherApp.sqlite")
        let enableAutoMigration = [NSMigratePersistentStoresAutomaticallyOption : true,
                                   NSInferMappingModelAutomaticallyOption : true]
        do {
            try self.persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: enableAutoMigration)
        } catch {
            fatalError("Failed to initialize persistentStoreCoordinator: \(error)")
        }
    }
    
    func execute(fetchRequest: NSFetchRequest<NSFetchRequestResult>) -> ([NSManagedObject]?) {
        var results: [NSManagedObject]? = nil
        managedObjectContext.performAndWait({
            do {
                results = try self.managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
            } catch {
                fatalError("Failed to perform fetch: \(error)")
            }
        })
        return results
    }
    
}

// MARK: - Constants

private extension CoreDataManager {
    
    struct Constants {
        static let fetchRequestAllUsers = "AllUsers"
    }
    
}
