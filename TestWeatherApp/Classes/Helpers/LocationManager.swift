//
//  LocationManager.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import MapKit
import Foundation
import CoreLocation

typealias CoordinateCompletionHandler = (_ location: CLLocation?) -> Void
typealias AuthStatusCompletionHandler = (_ status: CLAuthorizationStatus?) -> Void
typealias PlacemarkCompletionHandler = (_ placemark: CLPlacemark?) -> Void
typealias PlacemarkAddressCompletionHandler = (_ address: String?) -> Void
typealias PlacemarksCompletionHandler = (_ placemarks: [CLPlacemark]?) -> Void


final class LocationManager: NSObject {
    
    static let shared = LocationManager()
    
    fileprivate var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    var geocoder: CLGeocoder?
    var coordinateCompletionHandler: CoordinateCompletionHandler?
    var authStatusChangedHandler: AuthStatusCompletionHandler?
    
    // MARK: - Lifecycle
    
    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        
        geocoder = CLGeocoder()
    }
    
    // MARK: - Public
    
    func updateLocationWith(coordinateCompletionHandler: CoordinateCompletionHandler?, authStatusCompletionHandler: AuthStatusCompletionHandler?) {
        locationManager?.requestWhenInUseAuthorization()
        self.authStatusChangedHandler = authStatusCompletionHandler
        locationManager?.startUpdatingLocation()
        self.coordinateCompletionHandler = coordinateCompletionHandler
    }
    
    func retrievePlacemarkFrom(location: CLLocation, completion: @escaping PlacemarkCompletionHandler) {
        geocoder?.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            completion(placemarks?.first)
        })
    }
    
    func retrievePlacemarkFrom(coordinates: CLLocationCoordinate2D, completion: @escaping PlacemarkCompletionHandler) {
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        retrievePlacemarkFrom(location: location, completion: completion)
    }
    
    func retrievePlacemarkFrom(coordinates: CLLocationCoordinate2D, completionAddress: @escaping PlacemarkAddressCompletionHandler) {
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        retrievePlacemarkFrom(location: location) { (placemark) in
            if let pm = placemark {
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                completionAddress(addressString)
            }
        }
    }
    
    func retrieveLocationsFrom(stringLocation: String, completion: @escaping PlacemarksCompletionHandler) {
        let region = MKCoordinateRegionForMapRect(MKMapRectWorld)
        
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = stringLocation
        searchRequest.region = region
        
        let localSearch = MKLocalSearch(request: searchRequest)
        localSearch.start{ (response, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                completion(response?.mapItems.map{ $0.placemark })
            }
        }
    }
    
    func stopLocationUpdates() {
        locationManager?.stopUpdatingLocation()
        self.authStatusChangedHandler = nil
        self.coordinateCompletionHandler = nil
    }
    
    func cancelRequests() {
        geocoder?.cancelGeocode()
    }

}

// MARK: - CLLocationManagerDelegate

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.first
        locationManager?.stopUpdatingLocation()
        coordinateCompletionHandler?(currentLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.authStatusChangedHandler?(status)
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
        case .denied, .restricted:
            print("Auth status restricted")
        default:
            break
        }
    }
    
}
