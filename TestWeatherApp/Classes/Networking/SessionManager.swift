//
//  SessionManager.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class SessionManager: NSObject, NetworkModel {
    
    // MARK: - NetworkModel
    
    var restClient: RESTClient?
    var lastTask: URLSessionTask?
    var lastRestClientError: RESTClient.ErrorType?
    var onNoInternetConnection: ((_ request: URLRequest) -> Void)?
    var onRequestFailure: ((_ error: RESTClient.ErrorType, _ data: Data?, _ request: URLRequest) -> Void)?
    
    // MARK: - Properties
    
    var token: Token?
    var pushToken: String!
    var storedUser = User.lastUser()
    
    var isLoggedIn: Bool {
        return (token != nil)
    }
    
    var currentEmail: String? {
        return Credentials.loadFromKeyсhain()?.email
    }
    
    fileprivate(set) var authorizationInProgress = false
    
    // MARK: - Lifecycle
    
    init(restClient: RESTClient) {
        self.restClient = restClient
        super.init()
        cleanUpKeyChainAtFirstLaunch()
        token = Token.loadFromKeyсhain()
    }
    
    // MARK: - Private
    
    private func storeUser(with JSON: [String : Any], completion: NetworkModelCompletion? = nil) {
        var success = true
        if let stored = storedUser {
            stored.update(with: JSON)
        } else if let userModel = User.coreDataObject()  {
            userModel.update(with: JSON)
        } else {
            success = false
        }
        invoke(completionHandler: completion, success: success)
    }
    
    private func deleteUser() {
        User.deleteLastUser()
    }
}

// MARK: - Public

extension SessionManager {
    
    func login(with formModel: LoginModel, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.signInPath
            builder.method = .POST
            builder.body = .MappableObject(formModel, contentType: .ApplicationJSON)
        }, successHandler: { [weak self] (data, request, response) in
            self?.authorizationInProgress = false
            self?.handleAuthorizationResponse(data: data, request: request, response: response, completion: { (success) in
                self?.invoke(completionHandler: completionHandler, success: true)
            })
            }, failureHandler: { [weak self] (error, data, request, response) in
                self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        })
    }
    
    func signUp(with formModel: RegisterModel, completionHandler: NetworkModelCompletion? = nil) {
        authorizationInProgress = true
        perform(request: { (builder) in
            builder.path = Constants.signUpPath
            builder.method = .POST
            if let avatar = formModel.avatar {
                var params = [String : String]()
                params["username"] = formModel.toJSON()["username"] as! String
                params["email"] = formModel.toJSON()["email"] as! String
                params["password"] = formModel.toJSON()["password"] as! String
                builder.body = .MultipartFormData(params: params, URLs: nil, data: [UIImageJPEGRepresentation(avatar, 1.0)!], fileKey: "avatar")
            } else {
                builder.body = .MappableObject(formModel, contentType: .ApplicationJSON)
            }
        }, successHandler: { [weak self] (data, request, response) in
            guard let strongSelf = self else { return }
            strongSelf.authorizationInProgress = false
            strongSelf.handleAuthorizationResponse(data: data, request: request, response: response, completion: { (success) in
                strongSelf.invoke(completionHandler: completionHandler, success: true)
            })
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
    
    func signOut(completionHandler: NetworkModelCompletion? = nil) {
        self.cleanUp()
        self.invoke(completionHandler: completionHandler, success: true)
//        perform(request: { (builder) in
//            builder.path = Constants.signOutPath
//            builder.method = .PUT
//        }, successHandler: { [weak self] (data, request, response) in
//            
//        }) { [weak self] (error, data, request, response) in
//            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
//        }
    }
    
    func cleanUp() {
        deleteCookies()
        Token.deleteFromKeyсhain()
        token = nil
        Credentials.deleteFromKeyсhain()
        storedUser = nil
        DispatchQueue.main.async {
            User.deleteLastUser()
        }
    }
    
}

// MARK: - Private

private extension SessionManager {
    
    func handleAuthorizationResponse(data: Data?, request: URLRequest, response: URLResponse?, completion: NetworkModelCompletion? = nil) {
        guard let responseData = data, let token = Token(from: responseData) else {
            authorizationInProgress = false
            let error: RESTClient.ErrorType = response == nil ? .noHTTPResponse : .noAuthorizationToken
            self.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completion)
            return
        }
        
        DispatchQueue.main.async {
            self.token = token
            self.token?.storeToKeyсhain()
            
            self.restClient?.didLogin()
            completion?(true)
            if let JSON = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : Any] {
                if self.storedUser != nil {
                    self.storedUser!.update(with: JSON)
                    if let email = self.storedUser?.email {
                        let credentials = Credentials(email: email)
                        credentials.storeToKeyсhain()
                    }
                    completion?(true)
                } else {
                    if let user = User.coreDataObject() {
                        self.storedUser = user
                        self.storedUser!.update(with: JSON)
                        if let email = self.storedUser?.email {
                            let credentials = Credentials(email: email)
                            credentials.storeToKeyсhain()
                        }
                        completion?(true)
                    } else {
                        completion?(false)
                    }
                }
            } else {
                completion?(false)
            }
            
            
            self.authorizationInProgress = false
        }
    }
    
    func deleteCookies() {
        HTTPCookieStorage.shared.cookies?.forEach { HTTPCookieStorage.shared.deleteCookie($0) }
    }
    
    
    func cleanUpKeyChainAtFirstLaunch() {
        guard UserDefaults.standard.object(forKey: Constants.flagKey) != nil else {
            Token.deleteFromKeyсhain()
            Credentials.deleteFromKeyсhain()
            UserDefaults.standard.set(Constants.flagKey, forKey: Constants.flagKey)
            return
        }
    }
    
}

// MARK: - Token

extension SessionManager {
    
    struct Token {
        
        var string = ""
        
        init(_ authorizationToken: String) {
            self.string = authorizationToken
        }
        
        init?(from httpData: Data) {
            guard let JSON = try! JSONSerialization.jsonObject(with: httpData, options: .allowFragments) as? [String : Any], let authorizationToken = JSON["token"] as? String else {
                return nil
            }
            self.string = authorizationToken

        }
        
        func storeToKeyсhain() {
            KeychainWrapper.standard.set(string, forKey: Constants.authorizationTokenKey)
            print("> SessionManager: Toket.stored!")
        }
        
        static func loadFromKeyсhain() -> Token? {
            guard let authorizationToken = KeychainWrapper.standard.string(forKey: Constants.authorizationTokenKey) else { return nil }
            return Token(authorizationToken)
        }
        
        static func deleteFromKeyсhain() {
            KeychainWrapper.standard.removeObject(forKey: Constants.authorizationTokenKey)
        }
        
    }
    
}

// MARK: - Credentials

private extension SessionManager {
    
    struct Credentials {
        
        var email = ""
        
        func storeToKeyсhain() {
            KeychainWrapper.standard.set(email, forKey: Constants.emailKey)
        }
        
        static func loadFromKeyсhain() -> Credentials? {
            guard let email = KeychainWrapper.standard.string(forKey: Constants.emailKey) else { return nil }
            return Credentials(email: email)
        }
        
        static func deleteFromKeyсhain() {
            KeychainWrapper.standard.removeObject(forKey: Constants.emailKey)
        }
        
    }
    
}

// MARK: - Constants

private extension SessionManager {
    
    enum Constants {
        static let signInPath = "/login"
        static let signUpPath = "/create"
        static let signOutPath = "/logout"

        static let flagKey = "SessionManagerFlag"
        static let authorizationTokenKey = "authorizationToken"
        static let emailKey = "EmailKey"
    }
    
}
