//
//  RESTClient.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Foundation

final class RESTClient {
    
    typealias SuccessHandler = (_ data: Data?, _ request: URLRequest, _ response: URLResponse?) -> Void
    typealias FailureHandler = (_ error: ErrorType?, _ data: Data?, _ request: URLRequest, _ response: URLResponse?) -> Void
    
    // MARK: - Properties
    
    weak var sessionManager: SessionManager?
    let baseURL: URL
    
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration)
    }()
    
    #if DEBUG
    fileprivate var isDebugMode = true
    #else
    fileprivate var isDebugMode = false
    #endif
    
    fileprivate var lastError: RESTClient.ErrorType?
    fileprivate var pendingRequests: [URLRequest: (successHandler: SuccessHandler?, failureHandler: FailureHandler?)] = [:]
    fileprivate var resentRequests: Set<URLRequest> = []
    
    fileprivate var predefinedHeaders: [String: String] {
        var headers: [String: String] = [:]
        headers["Accept"] = "application/json"
        if let tokenString = self.sessionManager?.token?.string {
            headers[Constants.authorizationKey] = tokenString
        }
        
        return headers
    }
    
    // MARK: - Lifecycle
    
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    deinit {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
}

// MARK: - Public

extension RESTClient {
    
    func perform(request: URLRequest, successHandler: SuccessHandler?, failureHandler: FailureHandler?) -> URLSessionTask {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let task = session.dataTask(with: request) { [weak self] (data, response, error) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.hideNetworkActivityIndicatorIfNeeded()
            
            if strongSelf.isDebugMode {
                strongSelf.printRequest(request, response: response, responseData: data, printHeaders: true)
            }
            
            if let error = error {
                strongSelf.handleSystemError(error, data: data, request: request, response: response, failureHandler: failureHandler)
            } else {
                guard let HTTPResponse = response as? HTTPURLResponse else {
                    failureHandler?(ErrorType.noHTTPResponse, data, request, response)
                    return
                }
                
                if 0...299 ~= HTTPResponse.statusCode {
                    successHandler?(data, request, HTTPResponse)
                    strongSelf.resentRequests.remove(request)
                } else {
                    strongSelf.handleFailedHTTPResponse(data: data, request: request, response: HTTPResponse, successHandler: successHandler, failureHandler: failureHandler)
                }
            }
        }
        task.resume()
        return task
    }
    
    func perform(request requestBuilderClosure: (_ builder: URLRequestBuilder) -> Void, successHandler: SuccessHandler?, failureHandler: FailureHandler?) -> URLSessionTask? {
        let builder = URLRequestBuilder(baseURL: baseURL)
        requestBuilderClosure(builder)
        
        builder.headerFields += predefinedHeaders
        if let url = builder.url {
            clearCoookies(for: url)
        }
        
        if let request = builder.urlRequest {
            return perform(request: request, successHandler: successHandler, failureHandler: failureHandler)
        }
        
        return nil
    }
    
    func clearCoookies(for url: URL) {
        let cookisStorage = HTTPCookieStorage.shared
        let cookies = cookisStorage.cookies(for: url)
        for cookie in cookies ?? [] {
            cookisStorage.deleteCookie(cookie)
        }
    }
    
    func didLogin() {
        if !pendingRequests.isEmpty {
            let aPendingRequests = pendingRequests
            pendingRequests = [:]
            for (request, handlers) in aPendingRequests {
                resend(request: request, successHandler: handlers.successHandler, failureHandler: handlers.failureHandler)
            }
        }
        
    }
    
}

// MARK: - Private

private extension RESTClient {
    
    func resend(request: URLRequest, successHandler: SuccessHandler?, failureHandler: FailureHandler?) {
        var newRequest = request
        
        predefinedHeaders.forEach({ newRequest.setValue($1, forHTTPHeaderField: $0) })
        
        resentRequests.insert(newRequest)
        _ = perform(request: newRequest, successHandler: successHandler, failureHandler: failureHandler)
    }
    
    func handleSystemError(_ error: Error, data: Data?, request: URLRequest, response: URLResponse?, failureHandler: FailureHandler?) {
        if (error as NSError).code == NSURLErrorNotConnectedToInternet {
            lastError = ErrorType.notConnectedToInternet
            failureHandler?(ErrorType.notConnectedToInternet, data, request, response)
        } else if (error as NSError).code != NSURLErrorCancelled {
            lastError = ErrorType.systemError(error: error)
            failureHandler?(ErrorType.systemError(error: error), data, request, response)
        }
        resentRequests.remove(request)
    }
    
    func handleFailedHTTPResponse(data: Data?, request: URLRequest, response: HTTPURLResponse, successHandler: SuccessHandler?, failureHandler: FailureHandler?) {
        switch StatusCode(rawValue: response.statusCode) ?? .undefined {
        case .unauthorized:
            DispatchQueue.main.async {
                self.lastError = ErrorType.Unauthorized
                self.handleUnauthorizedResponse(for: request, with: successHandler, and: failureHandler)
            }
            
        default:
            handleDefaultError(data: data, request: request, response: response, successHandler: successHandler, failureHandler: failureHandler)
        }
    }
    
    func handleDefaultError(data: Data?, request: URLRequest, response: HTTPURLResponse, successHandler: SuccessHandler?, failureHandler: FailureHandler?) {
        var errorJSON: [String : AnyObject]?
        if let data = data, data.count > 0 {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let JSON = json as? [String : AnyObject] {
                    errorJSON = JSON
                }
            } catch {
                print("Serialisation error:\(error.localizedDescription)")
            }
            
        }
        
        lastError = ErrorType.serverError(errorJSON: errorJSON)
        failureHandler?(ErrorType.serverError(errorJSON: errorJSON), data, request, response)
        resentRequests.remove(request)
        
        if isDebugMode {
            print("Server error:\n\(data?.prettyJSONString ?? "Unknown server error")")
        }
    }
    
    func handleUnauthorizedResponse(for request: URLRequest, with successHandler: SuccessHandler?, and failureHandler: FailureHandler?) {
        if let error = lastError {
            switch error {
            case .Unauthorized:
                failureHandler?(RESTClient.ErrorType.Unauthorized, nil, request, nil)
                AppDelegate.shared.sessionManager.cleanUp()
                
            default: break
                
            }
        } else {
            
        }
        
    }
    
    func hideNetworkActivityIndicatorIfNeeded() {
        session.getTasksWithCompletionHandler { (dataTasks, _, _) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = dataTasks.count > 0
            }
            
        }
    }
    
    func printRequest(_ request: URLRequest?, response: URLResponse?, responseData: Data?, printHeaders: Bool) {
        print("{")
        if let URL = request?.url {
            print("\tURL: \(URL)")
        }
        if let method = request?.httpMethod {
            print("\tMethod: \(method)")
        }
        if let headers = request?.allHTTPHeaderFields, !headers.isEmpty && printHeaders {
            print("\tRequest Headers: \(headers)")
        }
        if let body = request?.httpBody {
            let bodyString = body.prettyJSONString.replacingOccurrences(of: "\n", with: "\n\t")
            print("\tBody:\n\t\(bodyString)")
        }
        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
            print("\tStatus code: \(statusCode)")
        }
        if let headers = (response as? HTTPURLResponse)?.allHeaderFields, !headers.isEmpty && printHeaders {
            print("\tResponse Headers: \(headers)")
        }
        if let data = responseData {
            let dataString = data.prettyJSONString.replacingOccurrences(of: "\n", with: "\n\t")
            print("\tResponse data:\n\t\(dataString)")
        }
        print("\n}")
    }
    
}

// MARK: - ErrorType

extension RESTClient {
    
    enum ErrorType: Error {
        case notConnectedToInternet
        case noHTTPResponse
        case Unauthorized
        case noAuthorizationToken
        case systemError(error: Error)
        case serverError(errorJSON: [String : AnyObject]?)
        
        var title: String {
            return NSLocalizedString("Sorry", comment: "")
        }
    }
    
}

extension RESTClient.ErrorType: CustomStringConvertible {
    
    var description: String {
        switch self {
        case .notConnectedToInternet: return NSLocalizedString("Your internet connection is currently unavailable. Please try your request again.", comment: "")
        case .noHTTPResponse: return NSLocalizedString("There was an error completing your request. Please try your request again.", comment: "")
        case .noAuthorizationToken: return NSLocalizedString("No authorization token in response.", comment: "")
        case .Unauthorized: return NSLocalizedString("Unauthorized", comment: "")
        case .systemError(let error): return error.localizedDescription
        case .serverError(let errorJSON):
            guard var message = errorJSON?["error"] as? String else {
                return NSLocalizedString("Unknown server error", comment:"")
            }
            
            message = message.replacingOccurrences(of: "\n", with: " ")
            
            let parameters = errorJSON?["parameters"]
            
            if let array = parameters as? [String] {
                for (index, value) in array.enumerated() {
                    let template = "%\(index + 1)"
                    message = message.replacingOccurrences(of: template, with: value)
                }
            } else if let dictionary = parameters as? [String: String] {
                for (key, value) in dictionary {
                    let template = "%\(key)"
                    message = message.replacingOccurrences(of: template, with: value)
                }
            }
            return message
        }
    }
    
}

// MARK: - Constants

private extension RESTClient {
    
    enum Constants {
        static let authorizationKey = "token"
    }
    
    enum StatusCode: Int {
        case undefined
        case unauthorized = 403
    }
    
}
