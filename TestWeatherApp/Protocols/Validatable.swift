//
//  Validatable.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Foundation

protocol Validatable {
 
    //implement in each validatable model
    func validate() throws
}
