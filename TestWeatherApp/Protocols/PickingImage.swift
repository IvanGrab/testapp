//
//  PickingImage.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import MobileCoreServices

protocol PickingImage: class {
    
    func pickingDidCancel()
    func didSelectMedia(with image: UIImage?, thumbnail: UIImage?)
    
}

extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePicker: UIImagePickerController? {
        get {
            return UIImagePickerController()
        }
        set {
            
        }
    }
    
    func startPicking(isCameraOnly: Bool, alertControllerTitle: String? = nil, libraryActionTitle: String? = nil, newPhotoActionTitle: String? = nil, deniedAccess: (()->())? = nil) {
    
        let libraryAction = UIAlertAction(title: NSLocalizedString(libraryActionTitle ?? "Select from library!", comment: ""), style: .default) { (action) in
            guard let picker = self.imagePicker else {
                fatalError("Oh nooo!")
                return
            }
           
            picker.delegate = (self as (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
            picker.sourceType = .photoLibrary
            picker.mediaTypes = [kUTTypeImage as String]
            picker.allowsEditing = true
            picker.title = NSLocalizedString("Take photo!", comment: "")
            
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    if let selfViewController = self as? UIViewController {
                        DispatchQueue.main.async {
                            selfViewController.present(picker, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        deniedAccess?()
                    }
                }
            })
            
        }
        
        let cameraAction = UIAlertAction(title: NSLocalizedString(newPhotoActionTitle ?? "Take photo", comment: ""), style: .default) { (action) in
            guard let picker = self.imagePicker else {
                fatalError("Oh nooo!")
                return
            }
            picker.delegate = (self as (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
            picker.sourceType = .camera
            picker.mediaTypes = [kUTTypeImage as String]
            picker.allowsEditing = true
            picker.title = NSLocalizedString("Take Photo", comment: "")
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if granted {
                    if let selfViewController = self as? UIViewController {
                        DispatchQueue.main.async {
                            selfViewController.present(picker, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        deniedAccess?()
                    }
                }
            })
            
        }
        
        if let selfViewController = self as? UIViewController, !isCameraOnly {
            ShowAlert.showAlertContrller(with: .actionSheet, title: alertControllerTitle, message: nil, actions: [libraryAction, cameraAction], in: selfViewController)
        } else if isCameraOnly {
            guard let picker = self.imagePicker else {
                fatalError("Oh nooo!")
                return
            }
            
            picker.delegate = (self as (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
            picker.sourceType = .camera
            picker.mediaTypes = [kUTTypeImage as String]
            picker.allowsEditing = true
            picker.title = NSLocalizedString("Take Photo", comment: "")
            
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if granted {
                    if let selfViewController = self as? UIViewController {
                        DispatchQueue.main.async {
                            selfViewController.present(picker, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        deniedAccess?()
                    }
                }
            })
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            if let pickingSelf = self as? PickingImage {
                pickingSelf.pickingDidCancel()
            }
        }
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        func finishPicking(with image: UIImage) {
            var resized = image
            if image.size.width >= Configuration.MediaConstants.maxUploadImageSize {
                resized = image.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
            }
            picker.dismiss(animated: true) {
                if let picking = self as? PickingImage {
                    picking.didSelectMedia(with: resized, thumbnail: resized)
                }
            }
        }
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            var fixedImage = image.fixedOrientation()
            if fixedImage.size.width > Configuration.MediaConstants.maxUploadImageSize {
                fixedImage = fixedImage.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
            }
            let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            let localPath: URL = documentDirectory.appendingPathComponent("attachment.jpg")
            
            do {
                let data = UIImagePNGRepresentation(fixedImage)
                try data?.write(to: localPath)
            } catch {
                print("> CreatePostTableViewController: Cant write image to file!")
            }
            
            let attachment = Attachment(withFileUrl: localPath, mediaType: .photo)
            finishPicking(with: fixedImage)
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            var fixedImage = image.fixedOrientation()
            if fixedImage.size.width > Configuration.MediaConstants.maxUploadImageSize {
                fixedImage = fixedImage.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
            }
            let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            let localPath: URL = documentDirectory.appendingPathComponent("attachment.jpg")
            
            do {
                let data = UIImagePNGRepresentation(fixedImage)
                try data?.write(to: localPath)
            } catch {
                print("> CreatePostTableViewController: Cant write image to file!")
            }
            
            let attachment = Attachment(withFileUrl: localPath, mediaType: .photo)
            finishPicking(with: fixedImage)
        } else if let mediaURL = info[UIImagePickerControllerMediaURL] as? URL {
            if let mediaType = info[UIImagePickerControllerMediaType] as? String {
                if mediaType == kUTTypeImage as String {
                    let attachment = Attachment(withAssetUrl: mediaURL, mediaType: .photo, thumbLoadCompletion: { (image) in
                        guard let image = image else { return }
                        finishPicking(with: image)
                    })
                }
            }
        } else if let chosenImageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            if let mediaType = info[UIImagePickerControllerMediaType] as? String {
                if mediaType == kUTTypeImage as String {
                    let attachment = Attachment(withAssetUrl: chosenImageURL, mediaType: .photo, thumbLoadCompletion: { (image) in
                        guard let image = image else { return }
                        finishPicking(with: image)
                    })
                }
            }
        }
    }
}
