//
//  Identifiable.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

protocol Identifiable: Equatable {
    
    associatedtype IdentifierType: ExpressibleByStringLiteral
    
    var identifier: IdentifierType? { get set }
    
}

extension Identifiable {
    
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return String(describing: lhs.identifier) == String(describing: rhs.identifier)
    }
    
}
