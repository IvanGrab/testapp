//
//  NetworkModel.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import Foundation

typealias NetworkModelCompletion = (_ success: Bool) -> Void

protocol NetworkModel: class {
    
    var restClient: RESTClient? { get }
    var lastTask: URLSessionTask? { get set }
    var lastRestClientError: RESTClient.ErrorType? { get set }
    var onNoInternetConnection: ((_ request: URLRequest) -> Void)? { get set }
    var onRequestFailure: ((_ error: RESTClient.ErrorType, _ data: Data?, _ request: URLRequest) -> Void)? { get set }
    
    func invoke(completionHandler: NetworkModelCompletion?, success: Bool)
    func defaultSuccessHandler(data: Data?, request: URLRequest, response: URLResponse?, completion: NetworkModelCompletion?)
    func defaultFailureHandler(error: RESTClient.ErrorType?, data: Data?, request: URLRequest, response: URLResponse?, completion: NetworkModelCompletion?)
    
}

extension NetworkModel {
    
    func invoke(completionHandler: NetworkModelCompletion?, success: Bool) {
        if let completionHandler = completionHandler {
            DispatchQueue.main.async { () -> Void in
                completionHandler(success)
            }
        }
    }
    
    func defaultSuccessHandler(data: Data?, request: URLRequest, response: URLResponse?, completion: NetworkModelCompletion?) {
        var success = true
        if let data = data, var mappableSelf = self as? Mappable, data.count > 0 {
            if let map = Map(withoutdataKeyData: data) {
                mappableSelf.mapping(map: map)
            } else {
                success = false
            }
        }
        invoke(completionHandler: completion, success: success)
    }
    
    func defaultFailureHandler(error: RESTClient.ErrorType?, data: Data?, request: URLRequest, response: URLResponse?, completion: NetworkModelCompletion?) {
        lastRestClientError = error
        if let error = error {
            DispatchQueue.main.async { [weak self] in
                switch error {
                case .notConnectedToInternet:
                    self?.onNoInternetConnection?(request)
                default:
                    self?.onRequestFailure?(error, data, request)
                }
            }
        }
        invoke(completionHandler: completion, success: false)
    }
    
    func perform(request requestBuilderClosure: (_ builder: URLRequestBuilder) -> Void, successHandler: RESTClient.SuccessHandler?, failureHandler: RESTClient.FailureHandler?) {
        assert(restClient != nil, "Inject RestClient")
        lastTask = restClient?.perform(request: requestBuilderClosure, successHandler: successHandler, failureHandler: failureHandler)
    }
    
    func perform(request requestBuilderClosure: (_ builder: URLRequestBuilder) -> Void, completionHandler: NetworkModelCompletion?) {
        assert(restClient != nil, "Inject RestClient")
        lastTask = restClient?.perform(request: requestBuilderClosure, successHandler: { [weak self] (data, request, response) in
            self?.defaultSuccessHandler(data: data, request: request, response: response, completion: completionHandler)
        }) { [weak self] (error, data, request, response) in
            self?.defaultFailureHandler(error: error, data: data, request: request, response: response, completion: completionHandler)
        }
    }
    
}

