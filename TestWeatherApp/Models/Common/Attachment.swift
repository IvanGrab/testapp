//
//  Attachment.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import Photos

final class Attachment: NSObject {

    var mediaType: MediaType = .photo

    var image: UIImage?
    var thumbnail: UIImage?
    
    var fileURL: URL?
    var data: Data?
    
    convenience init(withAssetUrl: URL, mediaType: MediaType, thumbLoadCompletion: ((_ image: UIImage?)->())? = nil) {
        self.init()
        self.mediaType = mediaType
        let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [withAssetUrl], options: nil)
        if let photo = fetchResult.firstObject {
            if mediaType == .photo {
                PHImageManager.default().requestImage(for: photo, targetSize: CGSize(width: Configuration.MediaConstants.maxUploadImageSize, height: Configuration.MediaConstants.maxUploadImageSize), contentMode: PHImageContentMode.default, options: nil, resultHandler: { (image, info) in
                    
                    var resized = image
                    if resized?.size.width ?? 0 > Configuration.MediaConstants.maxUploadImageSize {
                        resized = image?.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)
                    }
                    if let image = resized {
                        let documentDirectory = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
                        let localPath: URL = documentDirectory.appendingPathComponent("attachment.jpg")
                        
                        do {
                            let data = UIImagePNGRepresentation(image.fixedOrientation())
                            try data?.write(to: localPath)
                        } catch {
                            print("> CreatePostTableViewController: Cant write image to file!")
                        }
                        
                        self.fileURL = localPath
                        self.image = image
                        DispatchQueue.main.async {
                            self.thumbnail = image
                            thumbLoadCompletion?(image)
                        }
                    }
                })
            } else {
                
            }
        }
        
        
    }
    
    convenience init(withFileUrl: URL, mediaType: MediaType) {
        self.init()
        fileURL = withFileUrl
        do {
            data = try Data.init(contentsOf: withFileUrl)
        } catch {
            print("> Attachment: Cant read data from fileURL: \(withFileUrl.absoluteString) !")
        }
        self.mediaType = mediaType
        if mediaType == .photo, let data = data {
            if let image = UIImage(data: data) {
                var resized = image
                if resized.size.width > Configuration.MediaConstants.maxUploadImageSize {
                    resized = image.resizeTo(newWidth: Configuration.MediaConstants.maxUploadImageSize)!
                }
                self.image = resized
                self.thumbnail = image.resizeTo(newWidth: 100)
            }
        }
    }
    
}

extension Attachment {
    
    enum MediaType {
        case photo
        case video
        
        var mimeType: String {
            switch self {
            case .photo:
                return "image/jpeg"
            case .video:
                return "video/quicktime"
            }
        }
    }
    
}
