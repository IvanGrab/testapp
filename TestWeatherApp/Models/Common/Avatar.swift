//
//  Avatar.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class Avatar: BaseNetworkModel, Mappable, Identifiable {
    
    // MARK: - Properties
    
    var identifier: String?
    var relativePath: String?
    var imageURL: URL?
    var image: UIImage?
    var isAvatar = false
    
    // MARK: - Lifecycle
    
    override init() {
        super.init()
    }
    
    // MARK: - Mappable
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        identifier <- map["id"]
        relativePath <- map["image"]
    }

}

// MARK: - Public

extension Avatar {
    
}

// MARK: -  NSCopying

extension Avatar: NSCopying {
    
    func copy(with zone: NSZone? = nil) -> Any {
        let avatar = Avatar()
        avatar.identifier = identifier
        avatar.relativePath = relativePath
        avatar.imageURL = imageURL
        return avatar
    }
    
}

// MARK: - Constants

private extension Avatar {
    
    struct Constants {
        static let baseImageURL = AppDelegate.shared.restClient.baseURL.deletingLastPathComponent().absoluteString
    }
    
}
