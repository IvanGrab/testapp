//
//  WheatherItem.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreLocation

final class WheatherItem: BaseNetworkModel, Identifiable, Mappable {

    var identifier: String?
    
    var image: Avatar?
    var imageDescription: String?
    var hashtag: String?
    var address: String?
    var weather: String?
    var coordinates: CLLocationCoordinate2D?
    
    //MARK: - Lifecycle
    
    convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        let avatar = Avatar()
        avatar.relativePath <- map["smallImagePath"]
        image = avatar
        
        identifier <- map["id"]
        imageDescription <- map["description"]
        hashtag <- map["hashtag"]
        
        if map.mappingType == .fromJSON {
            weather <- map["parameters.weather"]
            address <- map["parameters.address"]
            var lat = 0.0
            var lng = 0.0
            lat <- map["parameters.latitude"]
            lng <- map["parameters.longitude"]
            coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        } else {
            var lat = "\(coordinates?.latitude ?? 0.0)"
            var lng = "\(coordinates?.longitude ?? 0.0)"
            lat <- map["latitude"]
            lng <- map["longitude"]
            
        }
    }
    
}

extension WheatherItem: Validatable {
    
    func validate() throws {
        guard let _ = image?.image else {
            throw ValueValidationError.required("Image is required!")
        }
        
        guard let _ = coordinates else {
            throw ValueValidationError.required("Location data is required! Please allow acces to location data, amd try again!")
        }
        
        guard !(imageDescription ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required("Image Description is required!")
        }
        
        guard !(hashtag ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            throw ValueValidationError.required("Hashtag is required!")
        }
    }
    
}


extension WheatherItem {
    
    struct Constants {
        static let allImagesPath  = "/all"
        static let addImagePath  = "/image"
        static let generateGIFPath  = "/gif"
    }
    
    static func getAllImages(with completion: @escaping ((_ images: [WheatherItem]?, _ error: Error?)->())) {
        let instance = WheatherItem()
        instance.perform(request: { (builder) in
            builder.path = Constants.allImagesPath
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            if let JSON = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : Any], let array = JSON["images"] as? [[String : Any]]{
                if let mapped = Mapper<WheatherItem>().mapArray(JSONArray: array) {
                    completion(mapped, nil)
                } else {
                    completion([], nil)
                }
            }
        }) { (error, data, request, response) in
            DispatchQueue.main.async {
                completion(nil, error)
            }
        }
    }
    
    func post(with completion: NetworkModelCompletion?) {
        perform(request: { (builder) in
            builder.path = Constants.addImagePath
            builder.method = .POST
            if let data = UIImageJPEGRepresentation(image!.image!, 1.0) {
                let body = self.toJSON() as! [String : String]
                builder.body = .MultipartFormData(params: body, URLs: nil, data: [data], fileKey: "image")
            }
        }, completionHandler: completion)
    }
    
    static func getGIFURL(with completion: @escaping ((_ url: String?, _ error: Error?)->())) {
        let instance = WheatherItem()
        instance.perform(request: { (builder) in
            builder.path = Constants.generateGIFPath
            builder.method = .GET
        }, successHandler: { (data, request, response) in
            if let JSON = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String : Any], let gifURL = JSON["gif"] as? String {
                completion(gifURL, nil)
            } else {
                completion(nil, nil)
            }
        }) { (error, data, request, response) in
            DispatchQueue.main.async {
                completion(nil, error)
            }
        }
    }
    
}
