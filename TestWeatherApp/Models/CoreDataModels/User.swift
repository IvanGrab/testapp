//
//  User.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreData

@objc(User)
class User: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: String(describing: User.self))
    }
    
    // MARK: - Properties

    @NSManaged public var email: String?
    @NSManaged public var username: String?
    @NSManaged public var imageURLString: String?
    @NSManaged public var imageData: Data?
    
    // MARK: - Computed
    
    var image: UIImage? {
        get {
            guard let imageData = imageData else { return nil }
            return UIImage(data: imageData)
        } set {
            guard let image = image else { return }
            imageData = UIImageJPEGRepresentation(image, 1)
        }
    }
    
    var avatar: Avatar {
        get {
            let avatar = Avatar()
            avatar.relativePath = imageURLString
            avatar.image = image
            return avatar
        } set {
            image = avatar.image
            imageURLString = avatar.relativePath
        }
    }
    
    // MARK: - Public
    
    func update(with json: [String : Any]) {
        if let email = json["email"] as? String {
            self.email = email
        }
        
        if let name = json["username"] as? String {
            self.username = name
        }
        
        if let imageUrl = json["avatar"] as? String {
            self.imageURLString = imageUrl
        }

        print("> CoreDataUser: User updated!")
        CoreDataManager.shared.saveContext()
    }
    
    // MARK: - Class methods
    
    class func coreDataObject() -> User? {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: String(describing: User.self), in: CoreDataManager.shared.managedObjectContext) else {
            return nil
        }
        let user = User(entity: entityDescription, insertInto: CoreDataManager.shared.managedObjectContext)
        return user
    }
    
    class func lastUser() -> User? {
        return allUsers()?.first
    }
    
    class func deleteLastUser() {
        guard let lastuser  = lastUser() else { return }
        CoreDataManager.shared.managedObjectContext.delete(lastuser)
        CoreDataManager.shared.saveContext()
    }
    
    class func allUsers() -> [User]? {
        return CoreDataManager.shared.fetchAllObjects(className: String(describing: User.classForCoder())) as? [User]
    }
    
}
