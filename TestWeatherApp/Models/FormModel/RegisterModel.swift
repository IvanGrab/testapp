//
//  RegisterModel.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit

final class RegisterModel: BaseNetworkModel {

    var email: String?
    var username: String?
    var password: String?
    var avatar: UIImage?
    
}

extension RegisterModel: Validatable {
    
    func validate() throws {
        guard !(username ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { throw ValueValidationError.required("Username cant be ampty!")}
        
        guard !(email ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { throw ValueValidationError.required("Email cant be ampty!")}
        
        do {
            try PasswordValidator().validate(password ?? "")
        }
    }
    
}

extension RegisterModel: Mappable {
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        if map.mappingType == .toJSON {
            username <- map["username"]
            email <- map["email"]
            password <- map["password"]
        }
    }
    
}
