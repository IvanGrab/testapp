//
//  LoginModel.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//
import UIKit

final class LoginModel: BaseNetworkModel {
    
    // MARK: - Properties
    var email: String?
    var password: String?
    
    init(email: String?, password: String?) {
        self.email = email
        self.password = password
    }
    
    override init() {
        super.init()
    }
    
}

extension LoginModel: Validatable {
    
    func validate() throws {
        guard !(email ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { throw ValueValidationError.required("Email cant be ampty!")}
        
        do {
            try PasswordValidator().validate(password ?? "")
        }
    }
    
}

extension LoginModel: Mappable {
    
    convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
    }
    
}


