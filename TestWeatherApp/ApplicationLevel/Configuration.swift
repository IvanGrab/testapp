//
//  Configuration.swift
//  TestWeatherApp
//
//  Created by Ivan Grab on 5/18/18.
//  Copyright © 2018 Ivan Grab. All rights reserved.
//

import UIKit
import CoreData

final class Configuration {
    
    // MARK: - Static Properties
    
    static let restAPIURL = URL(string: "http://api.doitserver.in.ua")!

    struct MediaConstants {
        static let maxUploadImageSize: CGFloat = CGFloat(1000)
    }
    
    // MARK: - Properties
    
    let deviceUUID = UIDevice.current.identifierForVendor
    
    private let restClient: RESTClient
    private let persistentContainer: NSPersistentContainer
    
    // MARK: - Initializers
    
    init(restClient: RESTClient, persistentContainer: NSPersistentContainer) {
        self.restClient = restClient
        self.persistentContainer = persistentContainer
    }
    
    // MARK: - Public Methods
    
    func configure() {
        configureDependencies()
    }
    
    // MARK: - Private Methods
    
    private func configureDependencies() {
        // configure other libs if needed
    }
    
}
